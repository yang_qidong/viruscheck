/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.26 : Database - sa_token_demo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sa_token_demo` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `sa_token_demo`;

/*Table structure for table `role_resource` */

DROP TABLE IF EXISTS `role_resource`;

CREATE TABLE `role_resource` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '注解',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `resource_id` bigint(20) NOT NULL COMMENT '资源id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_role_resource_id` (`role_id`,`resource_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `role_resource` */

insert  into `role_resource`(`id`,`role_id`,`resource_id`,`create_time`,`update_time`) values (1,1,1,'2021-05-07 18:19:05','2021-05-07 18:19:05'),(2,1,2,'2021-05-07 18:19:09','2021-05-07 18:19:09'),(3,1,3,'2021-05-07 18:19:15','2021-05-07 18:19:15'),(4,2,3,'2021-06-18 00:10:32','2021-06-18 00:10:32'),(5,2,2,'2021-06-18 00:10:36','2021-06-18 00:10:36'),(6,2,4,'2021-06-18 00:21:45','2021-06-18 00:21:45'),(7,3,4,'2021-06-18 00:21:51','2021-06-18 00:21:51');

/*Table structure for table `sys_resource` */

DROP TABLE IF EXISTS `sys_resource`;

CREATE TABLE `sys_resource` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '注解',
  `resource_name` varchar(32) NOT NULL DEFAULT '' COMMENT '资源名称',
  `url` varchar(200) DEFAULT '' COMMENT 'url',
  `resource_code` varchar(32) DEFAULT '' COMMENT '资源编码，鉴权使用',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父级id',
  `resource_type` char(1) NOT NULL DEFAULT '' COMMENT '资源类型,1目录，2菜单，3按钮',
  `is_disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用，0否，1是',
  `icon` varchar(32) DEFAULT '' COMMENT '图标',
  `order_no` varchar(32) DEFAULT '' COMMENT '排序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_resource_code` (`resource_code`) USING BTREE,
  KEY `idx_is_disable` (`is_disable`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `sys_resource` */

insert  into `sys_resource`(`id`,`resource_name`,`url`,`resource_code`,`parent_id`,`resource_type`,`is_disable`,`icon`,`order_no`,`create_time`,`update_time`) values (1,'系统管理','','role:sys',0,'1',0,'','','2021-05-07 18:17:20','2021-05-07 18:17:20'),(2,'角色管理','role/list','role:list',1,'2',0,'','','2021-05-07 18:17:35','2021-05-07 18:17:35'),(3,'角色新增','','role:add',2,'3',0,'','','2021-05-07 18:18:57','2021-05-07 18:18:57'),(4,'角色查看','','role:view',0,'',0,'','','2021-06-18 00:19:14','2021-06-18 00:19:14');

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '注解',
  `role_name` varchar(32) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_code` varchar(32) NOT NULL DEFAULT '' COMMENT '角色编码，鉴权使用',
  `description` varchar(100) DEFAULT '' COMMENT '描述',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_role_code` (`role_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`role_name`,`role_code`,`description`,`create_time`,`update_time`) values (1,'管理员','admin','所有权限','2021-05-07 18:16:29','2021-05-07 18:16:29'),(2,'测试用户','test','没有系统权限','2021-06-18 00:11:15','2021-06-18 00:11:15'),(3,'用户','user','查看权限','2021-06-18 00:20:24','2021-06-18 00:20:24');

/*Table structure for table `tb_student` */

DROP TABLE IF EXISTS `tb_student`;

CREATE TABLE `tb_student` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(52) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(52) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(52) COLLATE utf8_unicode_ci DEFAULT NULL,
  `className` varchar(52) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isDanger` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_student` */

insert  into `tb_student`(`id`,`name`,`sex`,`phone`,`className`,`isDanger`) values (1,'201841404130','男','18929149590','18软件1班',0),(2,'201841404131','男','18929149591','18软件1班',0),(3,'201841404132','男','18929149521','18软件1班',0),(4,'201841404133','男','18929349591','18软件1班',0),(5,'201841404134','男','12929149591','18软件1班',0),(6,'201841404135','男','18929141591','18软件1班',0),(7,'201841404136','男','18529149591','18软件1班',0);

/*Table structure for table `tb_temp` */

DROP TABLE IF EXISTS `tb_temp`;

CREATE TABLE `tb_temp` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `temperature` varchar(52) COLLATE utf8_unicode_ci DEFAULT NULL,
  `des` varchar(52) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sysDate` date DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `studentId` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `studentId` (`studentId`),
  CONSTRAINT `studentId` FOREIGN KEY (`studentId`) REFERENCES `tb_student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_temp` */

insert  into `tb_temp`(`id`,`temperature`,`des`,`sysDate`,`address`,`studentId`) values (2,'36','健康','2021-06-09','1',1),(5,'36','健康','2021-06-09','1',2),(6,'36','健康','2021-06-10','广东省东莞市',3),(8,'36','健康','2021-06-10','广东省东莞市',4),(16,'36','健康','2021-06-11','广东省东莞市',5),(17,'36','健康','2021-06-14','广东省东莞市',6),(18,'36','健康','2021-06-15','广东省东莞市',7),(19,'36','健康','2021-06-15','广东省东莞市',2),(20,'36','健康','2021-06-15','广东省东莞市',7),(21,'36','健康','2021-06-15','广东省东莞市',1),(22,'36','健康','2021-06-15','广东省东莞市',5),(23,'36','健康','2021-06-15','广东省东莞市',4),(24,'36','健康','2021-06-15','广东省东莞市',5),(25,'36','健康','2021-06-15','广东省东莞市',2),(26,'36','健康','2021-06-15','广东省东莞市',4),(27,'36','健康','2021-06-15','广东省东莞市',3),(28,'36','健康','2021-06-15','广东省东莞市',6),(29,'36','健康','2021-06-15','广东省东莞市',1),(30,'36','健康','2021-06-17','广东省东莞市',3),(31,'36','健康','2021-06-17','广东省东莞市',4),(32,'36','健康','2021-06-17','广东省东莞市',2),(33,'36','健康','2021-06-17','广东省东莞市',1),(34,'37','jiank','2021-06-18','广东省东莞市',7),(35,'35','jiankang','2021-06-18','广东省东莞市',6);

/*Table structure for table `user_info` */

DROP TABLE IF EXISTS `user_info`;

CREATE TABLE `user_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '注解',
  `user_name` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `user_pwd` varchar(64) NOT NULL DEFAULT '' COMMENT '用户密码',
  `salt` varchar(32) NOT NULL DEFAULT '' COMMENT '密盐',
  `is_disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用，0否，1是',
  `disable_time` datetime DEFAULT NULL COMMENT '禁用时间，到期解封',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `user_info` */

insert  into `user_info`(`id`,`user_name`,`user_pwd`,`salt`,`is_disable`,`disable_time`,`create_time`,`update_time`) values (1,'admin','8a8a0767fcd81f75e1826a3c34374bc96596e41410f5221650a4fe6e52dd698a','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-05-07 17:45:23','2021-05-07 17:45:23'),(2,'201841404136','e9be0dbbe304e8abaa1fa584bab4a8fee37704adb0c17fb1535d025ffc5d0873','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-17 08:28:32','2021-06-17 08:28:32'),(3,'dongzi','5f7b1ec9da7ea4d7211e75b7208970913d1e9ab389125528ed01520a0768a7de','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-18 00:07:44','2021-06-18 00:07:44'),(4,'201841404130','95d13c0ab50556ed6fd5a65179b6493c36092f9c7fc824b98fb3f1e3cbc6e76f','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-18 00:25:58','2021-06-18 00:25:58'),(5,'201841404131','d3039accd8e5b4b1b46a7532631ae5293fd3c526447e991dd2d20efdb85888e2','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-18 00:26:04','2021-06-18 00:26:04'),(6,'201841404132','4b6644c53b593bf1d1a88526015183e88dd377b97d8d169ef6ee5f513d025a8c','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-18 00:26:08','2021-06-18 00:26:08'),(7,'201841404133','84d028c7cd4630007025ee4cb551ac5eb41fc182dd5db19ea94b0e1d8dbb0837','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-18 00:26:16','2021-06-18 00:26:16'),(8,'201841404134','a1501e1c3cc2a2a1339e3f02f8d3d01202f90d69b2873a30e5181216e215c3d2','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-18 00:26:21','2021-06-18 00:26:21'),(9,'201841404135','92c35c7f16e1f0e5ba19a55dc50bdd935adb8b71f72094ae710d3e0a893a4008','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-18 00:26:36','2021-06-18 00:26:36'),(10,'','','01c72bacfe2d4be4908b73fd328ba1fc',0,NULL,'2021-06-18 00:26:48','2021-06-18 00:26:48');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '注解',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_user_role_id` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `user_role` */

insert  into `user_role`(`id`,`user_id`,`role_id`,`create_time`,`update_time`) values (1,1,1,'2021-05-07 18:16:43','2021-05-07 18:16:43'),(2,3,2,'2021-06-17 23:14:36','2021-06-17 23:14:36'),(3,2,3,'2021-06-18 00:24:12','2021-06-18 00:24:12'),(4,4,3,'2021-06-18 08:26:54','2021-06-18 08:26:54'),(5,5,3,'2021-06-18 08:26:58','2021-06-18 08:26:58'),(6,6,3,'2021-06-18 08:27:00','2021-06-18 08:27:00'),(7,7,3,'2021-06-18 08:27:05','2021-06-18 08:27:05'),(8,8,3,'2021-06-18 08:27:25','2021-06-18 08:27:25'),(9,9,3,'2021-06-18 08:27:28','2021-06-18 08:27:28');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
