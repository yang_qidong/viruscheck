package com.snut.viruscheck.service;


import com.alibaba.cola.dto.Response;
import com.snut.viruscheck.satoken.login.dto.LoginQry;

/**
 * @ClassName: LoginService.java
 * @Description:
 * @Author lw1throw
 * @date 2021/5/8
 */
public interface LoginService {

    /**
     * 用户登录
     *
     * @param loginQry
     * @return
     */
    Response login(LoginQry loginQry);
}
