package com.snut.viruscheck.service;


import com.alibaba.cola.dto.Response;
import com.snut.viruscheck.satoken.login.dto.LoginQry;
import com.snut.viruscheck.satoken.login.executor.query.LoginQryExe;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ClassName: LoginServiceImpl.java
 * @Description: 登录实现
 * @Author lw1throw
 * @date 2021/5/8
 */
@Component("LoginServiceImpl")
public class LoginServiceImpl implements LoginService {

    @Resource
    private LoginQryExe loginQryExe;

    @Override
    public Response login(LoginQry loginQry) {
        return loginQryExe.execute(loginQry);
    }
}