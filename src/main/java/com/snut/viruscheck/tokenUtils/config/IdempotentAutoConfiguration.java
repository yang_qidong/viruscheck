package com.snut.viruscheck.tokenUtils.config;


import com.snut.viruscheck.tokenUtils.annotation.EnableIdempotentConfig;
import com.snut.viruscheck.tokenUtils.aop.IdempotentAspect;
import com.snut.viruscheck.tokenUtils.util.RedisIdempotentUtils;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author hulk
 *
 * @see https://www.cnblogs.com/toov5/p/10312458.html
 */
@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
@ConditionalOnBean(annotation = EnableIdempotentConfig.class,value = RedisConnectionFactory.class )
@EnableConfigurationProperties(IdempotentProperties.class)
public class IdempotentAutoConfiguration {



   @Bean(value = "idempotentRedisTemplate")
   public RedisTemplate<String, Object> idempotentRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
      RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
      redisTemplate.setKeySerializer(new StringRedisSerializer());
      redisTemplate.setHashKeySerializer(new StringRedisSerializer());
      redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
      redisTemplate.setHashValueSerializer(new JdkSerializationRedisSerializer());
      redisTemplate.setConnectionFactory(redisConnectionFactory);
      return redisTemplate;
   }
  @Bean
  @ConditionalOnMissingBean
  public RedisIdempotentUtils redisIdempotentUtils(RedisTemplate<String, Object> idempotentRedisTemplate, IdempotentProperties properties){
      RedisIdempotentUtils redisIdempotentUtils = new RedisIdempotentUtils ();
      redisIdempotentUtils.setRedisTemplate(idempotentRedisTemplate);
      redisIdempotentUtils.setModule(properties.getModule());
      redisIdempotentUtils.setTimeout(properties.getExpireTime());
     return redisIdempotentUtils;
  }

   @Bean
   @ConditionalOnMissingBean
   public IdempotentAspect idempotentAspect (RedisIdempotentUtils redisIdempotentUtils){
      IdempotentAspect interceptor = new IdempotentAspect (redisIdempotentUtils);
      return  interceptor;
   }


}
