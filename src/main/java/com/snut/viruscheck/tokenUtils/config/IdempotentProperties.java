package com.snut.viruscheck.tokenUtils.config;


import com.snut.viruscheck.tokenUtils.enums.IdempotentType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * idempotent properties
 *
 * @author hulk
 * @version 1.0.0
 */
@Data
@ConfigurationProperties(prefix = IdempotentProperties.PREFIX)
public class IdempotentProperties {


    static final String PREFIX = "idempotent";

    private IdempotentType type = IdempotentType.IDEMPOTENT_HEAD;
    //单位秒
    private long expireTime = 60*60;

    private String module = "module";



}
