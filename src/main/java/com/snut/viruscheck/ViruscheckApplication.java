package com.snut.viruscheck;

import com.snut.viruscheck.tokenUtils.annotation.EnableIdempotentConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//(scanBasePackages = {"com.snut.satoken","com.alibaba.cola"})
//@ComponentScan(basePackages = { "com.snut.satoken" ,"com.snut.viruscheck" })
@SpringBootApplication
@EnableIdempotentConfig
public class ViruscheckApplication {

    public static void main(String[] args) {
        SpringApplication.run(ViruscheckApplication.class, args);
    }

}
