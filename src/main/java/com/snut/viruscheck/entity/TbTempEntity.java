package com.snut.viruscheck.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;
//, schema = "snut_virus"
@Entity
@Table(name = "tb_temp")
public class TbTempEntity {
    private int id;
    private String temperature;
    private String des;
    private Date sysDate;
    private String address;
    private int studentId;

    public TbTempEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "temperature")
    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    @Basic
    @Column(name = "des")
    public String getDesc() {
        return des;
    }

    public void setDesc(String des) {
        this.des = des;
    }

    @Basic
    @Column(name = "sysDate")
    public Date getSysDate() {
        return sysDate;
    }

    public void setSysDate(Date sysDate) {
        this.sysDate = sysDate;
    }


    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "studentId")
    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass () != o.getClass ()) return false;
        TbTempEntity that = (TbTempEntity) o;
        return id == that.id &&
                Objects.equals (temperature, that.temperature) &&
                Objects.equals (des, that.des) &&
                Objects.equals (sysDate, that.sysDate) &&
                Objects.equals (address, that.address) &&
                Objects.equals (studentId, that.studentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash (id, temperature, des, sysDate, address, studentId);
    }
}
