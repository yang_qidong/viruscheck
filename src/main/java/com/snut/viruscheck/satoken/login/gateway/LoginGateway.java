package com.snut.viruscheck.satoken.login.gateway;


import com.snut.viruscheck.satoken.login.model.Login;

/**
 * @ClassName: LoginGateway.java
 * @Description:
 * @Author lw1throw
 * @date 2021/5/8
 */
public interface LoginGateway {

    /**
     * 根据用户名查询
     * @param userName 用户名
     * @return
     */
    Login getByUserName(String userName);
}
