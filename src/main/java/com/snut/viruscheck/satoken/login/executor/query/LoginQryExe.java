package com.snut.viruscheck.satoken.login.executor.query;


import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.cola.dto.Response;
import com.snut.viruscheck.satoken.login.dto.LoginQry;
import com.snut.viruscheck.satoken.login.gateway.LoginGateway;
import com.snut.viruscheck.satoken.token.common.constant.DeviceEnum;
import com.snut.viruscheck.satoken.token.common.util.Md5Util;
import com.snut.viruscheck.satoken.login.model.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName: LoginQryExe.java
 * @Description:
 * @Author lw1throw
 * @date 2021/5/8
 */
@Component
public class LoginQryExe {

    @Autowired
    private LoginGateway loginGateway;

    public Response execute(LoginQry loginQry) {
        Login login = loginGateway.getByUserName(loginQry.getUserName());
        if (login.getDisable()) {
            return Response.buildFailure("A0202", "该账户已禁用");
        }
        //判断密码
        if (!login.getUserPwd().equals(Md5Util.sha256Password(loginQry.getUserName(), loginQry.getUserPwd(),
                login.getSalt()))) {
            return Response.buildFailure("A0210", "用户名或密码错误");
        }
        StpUtil.setLoginId(login.getId(), DeviceEnum.WEB.type);
        return Response.buildSuccess();
    }
}
