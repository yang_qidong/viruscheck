package com.snut.viruscheck.satoken.login.dto;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName: LoginCmd.java
 * @Description:
 * @Author lw1throw
 * @date 2021/5/8
 */
public class LoginQry {

    @NotBlank(message = "用户名不能为空")
    private String userName;

    @NotBlank(message = "登录密码不能为空")
    private String userPwd;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }
}
