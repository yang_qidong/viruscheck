//package com.snut.viruscheck.satoken.login.handler;
//
//import com.alibaba.cola.dto.Response;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.converter.HttpMessageNotReadableException;
//import org.springframework.http.converter.HttpMessageNotWritableException;
//import org.springframework.validation.BindingResult;
//import org.springframework.validation.FieldError;
//import org.springframework.web.bind.MethodArgumentNotValidException;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//
///**
// * Create By focusmedia<chenxue4076@163.com>
// * File Name ValidParamExceptionHandler.java
// * Created Date 2020/11/24
// * Created Time 9:15
// */
//@Slf4j
//@ControllerAdvice
//public class ValidParamExceptionHandler {
//
//
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    @ResponseBody
//    public Object handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
//        BindingResult bindingResult = ex.getBindingResult();
//        StringBuffer stringBuffer = new StringBuffer(bindingResult.getFieldErrors().size() * 16);
//        stringBuffer.append("无效参数：");
//        for( int i = 0; i < bindingResult.getFieldErrors().size(); ++i) {
//            if (i > 0) {
//                stringBuffer.append("，");
//            }
//            FieldError fieldError = bindingResult.getFieldErrors().get(i);
//            stringBuffer.append(fieldError.getField());
//            stringBuffer.append(":");
//            stringBuffer.append(fieldError.getDefaultMessage());
//        }
//        Response response = new Response();
//        response.setErrCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
//        response.setErrMessage(stringBuffer.toString());
//        response.setSuccess(false);
//        log.error( "handleMethodArgumentNotValidException "+ stringBuffer.toString());
//        return response;
//    }
//
//    @ExceptionHandler(HttpMessageNotReadableException.class)
//    @ResponseBody
//    public Object handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
//        String exMessage = ex.getMessage();
//        Response response = new Response();
//        response.setErrCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
//        response.setErrMessage(exMessage);
//        response.setSuccess(false);
//        log.error( "handleMethodArgumentNotValidException "+ exMessage);
//        return response;
//    }
//
//    @ExceptionHandler(HttpMessageNotWritableException.class)
//    @ResponseBody
//    public Object handleHttpMessageNotWritableException(HttpMessageNotWritableException ex) {
//        String exMessage = ex.getMessage();
//        Response response = new Response();
//        response.setErrCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
//        response.setErrMessage(exMessage);
//        response.setSuccess(false);
//        log.error( "handleHttpMessageNotWritableException "+ exMessage);
//        return response;
//    }
//}
