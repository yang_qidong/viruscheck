package com.snut.viruscheck.satoken.token.login;


import com.alibaba.cola.exception.BizException;
import com.snut.viruscheck.satoken.login.gateway.LoginGateway;
import com.snut.viruscheck.satoken.login.model.Login;
import com.snut.viruscheck.satoken.token.mapper.UserInfoMapper;
import com.snut.viruscheck.satoken.token.mapper.dataobject.UserInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @ClassName: LoginGatewayImpl.java
 * @Description:
 * @Author lw1throw
 * @date 2021/5/8
 */
@Component
public class LoginGatewayImpl implements LoginGateway {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Override
    public Login getByUserName(String userName) {
        UserInfo userInfo = userInfoMapper.getByUserName(userName);
        if (Objects.isNull(userInfo)){
            throw new BizException ("A0210","用户名或密码错误");
        }
        Login login = new Login ();
        BeanUtils.copyProperties(userInfo,login);
        return login;
    }
}
