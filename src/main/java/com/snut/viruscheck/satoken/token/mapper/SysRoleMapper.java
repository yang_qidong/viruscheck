package com.snut.viruscheck.satoken.token.mapper;


import com.snut.viruscheck.satoken.token.mapper.dataobject.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

    /**
     * 根据用户id查询角色标识
     * @param userId
     * @return
     */
    List<String> listRoleByUserId(@Param("userId") Long userId);
}
