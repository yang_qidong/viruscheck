package com.snut.viruscheck.satoken.token.mapper;


import com.snut.viruscheck.satoken.token.mapper.dataobject.SysResource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysResourceMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysResource record);

    int insertSelective(SysResource record);

    SysResource selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysResource record);

    int updateByPrimaryKey(SysResource record);

    /**
     * 根据用户id查询权限标识
     * @param userId 用户id
     * @return
     */
    List<String> listPermissionByUserId(@Param("userId") Long userId);
}
