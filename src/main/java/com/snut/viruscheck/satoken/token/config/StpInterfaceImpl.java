package com.snut.viruscheck.satoken.token.config;


import cn.dev33.satoken.stp.StpInterface;
import com.snut.viruscheck.satoken.token.mapper.SysResourceMapper;
import com.snut.viruscheck.satoken.token.mapper.SysRoleMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName: StpInterfaceImpl.java
 * @Description:
 * @Author lw1throw
 * @date 2021/5/7
 */
@Component
public class StpInterfaceImpl implements StpInterface {

    @Resource
    private SysResourceMapper resourceMapper;
    @Resource
    private SysRoleMapper roleMapper;

    /**
     * 返回权限标识集合
     *
     * @param userId   用户ID
     * @param loginKey 登录key
     * @return
     */
    @Override
    public List<String> getPermissionList(Object userId, String loginKey) {
        List<String> list = resourceMapper.listPermissionByUserId(Long.valueOf(userId.toString()));
        return list;
    }

    /**
     * 返回角色集合
     *
     * @param userId   用户ID
     * @param loginKey
     * @return
     */
    @Override
    public List<String> getRoleList(Object userId, String loginKey) {
        List<String> list = roleMapper.listRoleByUserId(Long.valueOf(userId.toString()));
        return list;
    }
}
