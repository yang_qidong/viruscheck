package com.snut.viruscheck.satoken.token.mapper.dataobject;


import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * SysResource
 * @author lw
 */
public class SysResource implements Serializable {
    /**
     * 注解
     */
    private Long id;

    /**
     * 资源名称
     */
    private String resourceName;

    /**
     * url
     */
    private String url;

    /**
     * 资源编码，鉴权使用
     */
    private String resourceCode;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 资源类型,1目录，2菜单，3按钮
     */
    private String resourceType;

    /**
     * 是否禁用，0否，1是
     */
    private Boolean disable;

    /**
     * 图标
     */
    private String icon;

    /**
     * 排序号
     */
    private String orderNo;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
