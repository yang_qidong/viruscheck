package com.snut.viruscheck.satoken.token.mapper;

import com.snut.viruscheck.satoken.token.mapper.dataobject.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author lw
 */
@Mapper
public interface UserInfoMapper {
    /**
     * 根据用户名查询
     * @param userName
     * @return
     */
    UserInfo getByUserName(@Param("userName") String userName);

    int deleteByPrimaryKey(Long id);

    int insert(UserInfo record);

    int insertSelective(UserInfo record);

    UserInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserInfo record);

    int updateByPrimaryKey(UserInfo record);

}
