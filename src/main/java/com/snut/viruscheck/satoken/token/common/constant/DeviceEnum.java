package com.snut.viruscheck.satoken.token.common.constant;

/**
 * @ClassName: DeviceEnum.java
 * @Description: 设备类型
 * @Author lw1throw
 * @date 2021/5/8
 */
public enum DeviceEnum {
    WEB("WEB"),
    MOBILE( "MOBILE"),
    WAP("WAP");

    public String type;

    DeviceEnum(String type) {
        this.type = type;
    }
}
