package com.snut.viruscheck.satoken.token.common.util;


import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;

/**
 * @author lw1throw
 * @ClassName: Md5Util.java
 * @Description: TODO(Md5, sha加密工具类)
 * @date 2020年2月25日
 */
public class Md5Util {

    private static final Logger log = LoggerFactory.getLogger(Md5Util.class);

    private Md5Util() {
        super();
    }

    private static final String MD5_PREFIX = "lw1throw.com";

    /**
     * MD5加密
     *
     * @param data 待加密数据
     * @return
     */
    public static String Md5(String data) {
        return DigestUtils.md5Hex(data);
    }

    /**
     * MD5加密，带前缀
     *
     * @param data
     * @return
     */
    public static String Md5Prefix(String data) {
        return DigestUtils.md5Hex(MD5_PREFIX + data);
    }

    /**
     * sha256加密
     *
     * @param data 待加密数据
     * @return
     */
    public static String sha256(String data) {
        return DigestUtils.sha256Hex(data);
    }

    /**
     * sha256加密
     *
     * @param data 待加密数据
     * @return
     */
    public static String sha512(String data) {
        return Base64.getEncoder().encodeToString(DigestUtils.sha512(data));
    }

    /**
     * 使用sha256对密码加密
     *
     * @param userName 用户名
     * @param password 密码原文
     * @param salt     密盐
     * @return
     */
    public static String sha256Password(String userName, String password, String salt) {
        String data = MD5_PREFIX + userName + password + salt;
        return sha256(data);
    }
}
