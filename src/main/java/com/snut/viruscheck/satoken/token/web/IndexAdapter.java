package com.snut.viruscheck.satoken.token.web;


import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @ClassName: IndexAdapter.java
 * @Description:
 * @Author liaowei
 * @date 2021/5/11
 */
@Controller
public class IndexAdapter {

    @SaCheckLogin
    @GetMapping({"/index", "index.html"})
    public String index(Model model) {
        if (StpUtil.isLogin()) {
            SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
            model.addAttribute("tokenInfo", tokenInfo);
            return "/index";
        }
        return "/login";
    }
}
