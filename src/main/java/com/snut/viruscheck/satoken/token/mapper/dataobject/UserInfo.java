package com.snut.viruscheck.satoken.token.mapper.dataobject;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * user_info
 * @author 
 */
public class UserInfo implements Serializable {
    /**
     * 注解
     */
    private Long id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户密码
     */
    private String userPwd;

    /**
     * 密盐
     */
    private String salt;

    /**
     * 是否禁用，0否，1是
     */
    private Boolean disable;

    /**
     * 禁用时间，到期解封
     */
    private LocalDateTime disableTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    public LocalDateTime getDisableTime() {
        return disableTime;
    }

    public void setDisableTime(LocalDateTime disableTime) {
        this.disableTime = disableTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
