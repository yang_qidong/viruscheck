package com.snut.viruscheck.satoken.token.web;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.cola.dto.Response;
import com.snut.viruscheck.service.LoginService;
import com.snut.viruscheck.satoken.login.dto.LoginQry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
/**
 * @ClassName: LoginCOntroller.java
 * @Description:
 * @Author lw1throw
 * @date 2021/5/7
 */
@Controller
public class LoginAdaptor {

    private static final Logger log = LoggerFactory.getLogger(LoginAdaptor.class);

    @Autowired
    private LoginService loginService;

//    @PostMapping("/login")
//    @ResponseBody
//    public Response login(@RequestBody LoginQry loginQry) {
//        return loginService.login(loginQry);
//    }

    @SaCheckLogin
    @GetMapping("/check_login")
    @ResponseBody
    public Response checkLogin(){
        log.debug("登录用户id:{}", StpUtil.getLoginIdAsLong());
        log.debug("token：{}",StpUtil.getTokenInfo().toString());
        return Response.buildSuccess();
    }

    @SaCheckRole("admin")
    @GetMapping("/check_role")
    @ResponseBody
    public Response checkRole(){
        log.debug("登录用户id:{}",StpUtil.getLoginIdAsLong());
        log.debug("token：{}",StpUtil.getTokenInfo().toString());
        return Response.buildSuccess();
    }

    @SaCheckPermission("role:add")
    @GetMapping("/check_permission")
    @ResponseBody
    public Boolean checkPermission(){
//        boolean hasrole=true;
//        m.addAttribute ("test",hasrole);
        log.debug("登录用户id:{}",StpUtil.getLoginIdAsLong());
        log.debug("token：{}",StpUtil.getTokenInfo().toString());
//        return Response.buildSuccess();
        return true;
    }


    @GetMapping("/logout")
    @ResponseBody
    public Response logout(){
        log.debug("退出登录");
        StpUtil.logout();
        return Response.buildSuccess();
    }



}
