package com.snut.viruscheck.satoken.token.config;

import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName: SaTokenConfigure.java
 * @Description: 注解式鉴权，拦截器模式
 * @Author lw1throw
 * @date 2021/5/8
 */
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SaAnnotationInterceptor ()).addPathPatterns("/**").excludePathPatterns("/login-2",
                "/index", "/index.html");
    }
}
